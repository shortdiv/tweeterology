import { Component } from 'angular2/core';
import { bootstrap } from 'angular2/platform/browser';

@Component({
  selector: 'my-app',
  template: '<h1>Hello World!</h1>'
})

class AppComponent {

}

bootstrap(AppComponent);
